## 1.2.0

- Fix #3: reload config without restarting server
- Close #4: Add cooldown between two fastwrite

## 1.1.0

- Add support for colors in config

## 1.0.1

- Fix #1

## 1.0.0

- Add "/fastwriteadd" command
- Add "/fastwrite <text>" command