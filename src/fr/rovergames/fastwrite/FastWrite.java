package fr.rovergames.fastwrite;

import fr.rovergames.fastwrite.configurations.Config;
import fr.rovergames.fastwrite.managers.CommandManager;
import fr.rovergames.fastwrite.managers.FastWriteManager;
import fr.rovergames.fastwrite.managers.ListenerManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class FastWrite extends JavaPlugin {

    private Config config;

    private FastWriteManager manager;

    @Override
    public void onEnable() {
        CommandManager.init(this);
        ListenerManager.init(this);

        this.loadConfig();

        this.manager = new FastWriteManager(this);
    }

    public FastWriteManager getManager() {
        return this.manager;
    }

    @Override
    public FileConfiguration getConfig() {
        return this.config.getFileConfig();
    }

    public void loadConfig() {
        this.config = new Config("config", this.getDataFolder());
    }

    public String getMessage(String path) {
        return this.getConfig().getString(path).replace("&", "§");
    }
}
