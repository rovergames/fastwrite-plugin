package fr.rovergames.fastwrite.tasks;

import fr.rovergames.fastwrite.FastWrite;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ShowTextTask extends BukkitRunnable {

    private FastWrite plugin;

    private Player sender;
    private String text;

    private int timer;

    public ShowTextTask(FastWrite plugin, Player sender, String text) {
        this.plugin = plugin;

        this.sender = sender;
        this.text = text;

        this.timer = this.plugin.getConfig().getInt("timers.before_show_text");
    }

    @Override
    public void run() {
        this.timer--;

        if (timer == 0) {
            Bukkit.broadcastMessage(this.plugin.getMessage("prefix") + String.format(this.plugin.getMessage("messages.show_text"), this.plugin.getManager().getCurrentText()));
            this.cancel();
        }
    }
}
