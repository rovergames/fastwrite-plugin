package fr.rovergames.fastwrite.tasks;

import fr.rovergames.fastwrite.FastWrite;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class RewardingTask extends BukkitRunnable {

    private FastWrite plugin;

    private Player winner;

    private int timer;

    public RewardingTask(FastWrite plugin, Player winner) {
        this.plugin = plugin;

        this.winner = winner;

        this.timer = this.plugin.getConfig().getInt("timers.before_rewarding");
    }

    @Override
    public void run() {
        this.timer--;

        if (timer == 0) {
            this.plugin.getManager().getItems().forEach(item -> this.winner.getInventory().addItem(item));
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), String.format("eco give %s %s", this.winner.getDisplayName(), this.plugin.getManager().getMoney()));

            this.cancel();
        }
    }
}
