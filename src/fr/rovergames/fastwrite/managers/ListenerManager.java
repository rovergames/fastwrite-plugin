package fr.rovergames.fastwrite.managers;

import fr.rovergames.fastwrite.FastWrite;
import fr.rovergames.fastwrite.Listeners.AsyncPlayerChat;

public class ListenerManager {

    public static void init(FastWrite plugin) {
        plugin.getServer().getPluginManager().registerEvents(new AsyncPlayerChat(plugin), plugin);
    }
}
