package fr.rovergames.fastwrite.managers;

import fr.rovergames.fastwrite.FastWrite;
import fr.rovergames.fastwrite.tasks.RewardingTask;
import fr.rovergames.fastwrite.tasks.ShowTextTask;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class FastWriteManager {

    private FastWrite plugin;

    private String currentText;

    private ArrayList<ItemStack> items;
    private double money;

    private long lastFastWrite = 0;

    public FastWriteManager(FastWrite plugin) {
        this.plugin = plugin;

        this.currentText = null;

        this.items = (ArrayList<ItemStack>) this.plugin.getConfig().getList("rewards.items");
        this.money = this.plugin.getConfig().getDouble("rewards.money");
    }

    public void start(Player sender, String text) {
        new ShowTextTask(this.plugin, sender, text).runTaskTimer(this.plugin, 20L, 20L);

        this.currentText = text;
        this.lastFastWrite = System.currentTimeMillis();
    }

    public void stop(Player winner) {
        Bukkit.broadcastMessage(this.plugin.getMessage("prefix") + String.format(this.plugin.getMessage("messages.show_winner"), winner.getDisplayName()));
        winner.sendMessage(this.plugin.getMessage("prefix") + String.format(this.plugin.getMessage("messages.before_rewarding"), plugin.getConfig().getInt("timers.before_rewarding")));

        this.currentText = null;

        new RewardingTask(this.plugin, winner).runTaskTimer(this.plugin, 20L, 20L);
    }

    public boolean canStartFastWrite() {
        return System.currentTimeMillis() - this.lastFastWrite >= this.plugin.getConfig().getInt("timers.cooldown") * 1000L;
    }

    public long getLastFastWrite() {
        return this.lastFastWrite;
    }

    public boolean hasStarted() {
        return this.currentText != null;
    }

    public String getCurrentText() {
        return this.currentText;
    }

    public double getMoney() {
        return this.money;
    }

    public ArrayList<ItemStack> getItems() {
        return this.items;
    }

    public void addItem(ItemStack item) {
        this.items.add(item);

        this.plugin.loadConfig();
        this.plugin.getConfig().set("rewards.items", this.items);
        this.plugin.saveConfig();
    }
}
