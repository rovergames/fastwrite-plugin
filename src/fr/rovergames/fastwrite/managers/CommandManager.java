package fr.rovergames.fastwrite.managers;

import fr.rovergames.fastwrite.FastWrite;
import fr.rovergames.fastwrite.commands.FastWriteAddCommand;
import fr.rovergames.fastwrite.commands.FastWriteCommand;

public class CommandManager {

    public static void init(FastWrite plugin) {
        plugin.getCommand("fastwrite").setExecutor(new FastWriteCommand(plugin));
        plugin.getCommand("fastwriteadd").setExecutor(new FastWriteAddCommand(plugin));
    }
}
