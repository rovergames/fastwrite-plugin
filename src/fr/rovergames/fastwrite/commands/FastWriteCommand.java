package fr.rovergames.fastwrite.commands;

import fr.rovergames.fastwrite.FastWrite;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FastWriteCommand implements CommandExecutor {

    private FastWrite plugin;

    public FastWriteCommand(FastWrite plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player player = (Player) sender;

        if (!player.hasPermission("fastwrite.start")) return true;

        if (this.plugin.getManager().hasStarted()) {
            player.sendMessage(this.plugin.getMessage("messages.errors.already_started_fastwrite"));
            return true;
        } else if (!this.plugin.getManager().canStartFastWrite()) {
            player.sendMessage(String.format(this.plugin.getMessage("messages.errors.cooldown"), (this.plugin.getConfig().getInt("timers.cooldown") * 1000 - (System.currentTimeMillis() - this.plugin.getManager().getLastFastWrite())) / 1000 / 60));
            return true;
        }

        if (args.length == 0) return false;

        String text = StringUtils.join(args, ' ');

        this.plugin.getManager().start(player, text);

        Bukkit.broadcastMessage(this.plugin.getMessage("prefix") + String.format(this.plugin.getMessage("messages.start"), player.getDisplayName()));
        return true;
    }
}
