package fr.rovergames.fastwrite.commands;

import fr.rovergames.fastwrite.FastWrite;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class FastWriteAddCommand implements CommandExecutor {

    private FastWrite plugin;

    public FastWriteAddCommand(FastWrite plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player player = (Player) sender;

        if (!player.hasPermission("fastwrite.add")) return true;


        ItemStack item = player.getItemInHand();
        if (item.getType() != Material.AIR) {
            this.plugin.getManager().addItem(item);
            player.sendMessage(this.plugin.getMessage("prefix") + this.plugin.getMessage("messages.added"));
        } else {
            player.sendMessage(this.plugin.getMessage("prefix") + this.plugin.getMessage("messages.errors.add"));
        }

        return true;
    }
}
