package fr.rovergames.fastwrite.Listeners;

import fr.rovergames.fastwrite.FastWrite;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChat implements Listener {

    private final FastWrite plugin;

    public AsyncPlayerChat(FastWrite plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        System.out.println(event.getMessage());
        if (this.plugin.getManager().hasStarted()) {
            String msg = event.getMessage();

            if (msg.equals(this.plugin.getManager().getCurrentText())) {
                System.out.println("oui");
                plugin.getManager().stop(event.getPlayer());
            }
        }
    }
}
