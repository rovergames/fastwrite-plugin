package fr.rovergames.fastwrite.configurations;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class YamlConfig {
    private String nameFile;
    private File file;
    private FileConfiguration fileConfig;
    private boolean created = true;

    public YamlConfig(String nameFile, File folder){
        this.setNameFile(nameFile);
        this.setFile(new File(folder + "/" + this.getNameFile() + ".yml"));
        if(!folder.exists()){
            folder.mkdir();
        }
        if(!this.getFile().exists()){
            this.setCreated(false);
            try {
                this.getFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.setFileConfig(YamlConfiguration.loadConfiguration(this.getFile()));
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setFileConfig(FileConfiguration fileConfig) {
        this.fileConfig = fileConfig;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public File getFile(){
        return this.file;
    }

    public String getNameFile(){
        return this.nameFile;
    }

    public FileConfiguration getFileConfig(){
        return this.fileConfig;
    }

    public void save(){
        try {
            this.getFileConfig().save(this.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isCreated(){
        return this.created;
    }
}
