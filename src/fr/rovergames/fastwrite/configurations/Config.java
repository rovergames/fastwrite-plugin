package fr.rovergames.fastwrite.configurations;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.Arrays;

public class Config extends YamlConfig {

    public Config(String nameFile, File folder) {
        super(nameFile, folder);
        
        if (!this.isCreated()) this.setDefaults();
    }

    private void setDefaults() {
        this.getFileConfig().set("prefix", "[FastWrite]");

        this.getFileConfig().set("timers.before_show_text", 10);
        this.getFileConfig().set("timers.before_rewarding", 10);
        this.getFileConfig().set("timers.cooldown", 60 * 60);

        this.getFileConfig().set("rewards.items", Arrays.asList(new ItemStack(Material.DIAMOND, 32), new ItemStack(Material.GOLD_AXE, 1)));
        this.getFileConfig().set("rewards.money", 50);

        this.getFileConfig().set("messages.errors.already_started_fastwrite", "Un fastwrite a déjà été lancé !");
        this.getFileConfig().set("messages.errors.add", "Tu n'as pas d'item en main !");
        this.getFileConfig().set("messages.errors.cooldown", "Tu dois encore attendre %smin avant de pouvoir relancer un fastwrite !");

        this.getFileConfig().set("messages.start", "%s à lancé un event FastWrite... A vos claviers !");
        this.getFileConfig().set("messages.show_text", "Vous devez réécrire le texte suivant : %s");
        this.getFileConfig().set("messages.show_winner", "Bravo à %s qui gagne le FastWrite");
        this.getFileConfig().set("messages.before_rewarding", "Tu vas recevoir ta récompense dans %s secondes !");
        this.getFileConfig().set("messages.added", "Item ajouté !");

        this.save();
    }
}
